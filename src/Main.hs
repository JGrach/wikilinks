{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Network.HTTP.Types (methodGet)

import qualified Network (matchApiRoute, run, Application)
import Controller(ctrlWikiLinks, ctrlMissingRoute)

main :: IO ()
main = Network.run 3001 app

app::Network.Application
app request respond 
    | Network.matchApiRoute methodGet "wikilinks" request = ctrlWikiLinks request respond
    | otherwise = ctrlMissingRoute request respond
