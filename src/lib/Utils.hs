{-# LANGUAGE OverloadedStrings #-}

module Utils (Utils.compare, Utils.splitOn, Utils.flat, Utils.removeDuplicates, Utils.join) where
import Data.Text
import qualified Data.List 

compare:: Eq a => [a] -> [a] -> Bool
compare [] [] = True
compare [] _ = False
compare _ [] = False
compare a b = (Prelude.head a == Prelude.head b) && Utils.compare (Prelude.tail a) (Prelude.tail b)

splitOn:: Text -> Text -> [Text]
splitOn _ "" = []
splitOn "" text = [text]
splitOn delimiter text = Prelude.filter notEmptyString $ delimiter `Data.Text.splitOn` text

notEmptyString::Text -> Bool
notEmptyString "" = False
notEmptyString _ = True

flat :: (Monad m, Monad n) => m (n (n a)) -> m (n a)
flat x = (>>= id) `fmap` x 

removeDuplicates :: (Ord a) => [a] -> [a]
removeDuplicates = Prelude.map Prelude.head . Data.List.group . Data.List.sort

join::[Text] -> Text -> Text
join [] _ = ""
join [x] _ = x
join (x:xs) separator = x <> separator <> join xs separator
