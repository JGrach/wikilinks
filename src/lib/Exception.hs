{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE InstanceSigs #-}

module Exception(
    ExceptionFlag (ClientNetworkException, ClientStatusException, ParameterBadFormatException, NotFoundException, InternalException),
    fromExceptionFlagToJson
) where
import Data.ByteString (ByteString)
import Data.ByteString.Char8 (unpack)

data ExceptionFlag = ClientNetworkException
    | ClientStatusException
    | NotFoundException
    | ParameterBadFormatException
    | InternalException

fromExceptionFlagToJson::ExceptionFlag -> ByteString
fromExceptionFlagToJson ClientNetworkException = "CLIENT_NETWORK_EXCEPTION"
fromExceptionFlagToJson ClientStatusException = "CLIENT_STATUS_EXCEPTION"
fromExceptionFlagToJson ParameterBadFormatException = "PARAMETER_BAD_FORMAT_EXCEPTION"
fromExceptionFlagToJson NotFoundException = "NOT_FOUND_EXCEPTION"
fromExceptionFlagToJson InternalException = "INTERNAL_SERVER_EXCEPTION"

instance Show ExceptionFlag where
    show :: ExceptionFlag -> String
    show x = unpack $ fromExceptionFlagToJson x