{-# LANGUAGE OverloadedStrings #-}

module Network (
    matchApiRoute, 
    Network.Wai.Application, 
    Network.Wai.Handler.Warp.run,
    Network.Wai.Request,
    Network.Wai.Response,
    queryString,
    responseLBS
) where

import Network.HTTP.Types (Method)
import Network.Wai (Request, Response, Request (queryString), responseLBS, rawPathInfo, requestMethod, Application)
import Network.Wai.Handler.Warp (run)
import Data.Text (Text)

import qualified Utils (compare, splitOn)
import Data.Text.Encoding (decodeUtf8)

matchApiRoute:: Method -> Text -> Request -> Bool
matchApiRoute method path request = matchMethod method request && matchRoute path request

matchRoute:: Text -> Request -> Bool
matchRoute path request = Utils.compare (Utils.splitOn "/" path) (Utils.splitOn "/" (decodeUtf8 (rawPathInfo request)))

matchMethod:: Method -> Request -> Bool
matchMethod method request = requestMethod request == method
