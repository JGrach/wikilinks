module HttpClient(fetch) where

import Data.Text(Text, unpack)
import Network.HTTP.Client(httpLbs, newManager, parseRequest, Response, responseStatus, responseBody)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Data.ByteString.Lazy (ByteString)
import Network.HTTP.Types (Status(statusCode))
import Exception (ExceptionFlag (ClientStatusException))

fetch::Text -> IO (Either ExceptionFlag ByteString)
fetch link = parseResponse `fmap` getClient link

getClient::Text -> IO(Response ByteString)
getClient link = do {
    manager <- newManager tlsManagerSettings ;
    request <- parseRequest $ Data.Text.unpack link ;
    httpLbs request manager
}

parseResponse::Response ByteString -> Either ExceptionFlag ByteString
parseResponse response = let status = statusCode $ responseStatus response in 
    if status >= 200 && status < 300 then Right $ responseBody response
    else Left ClientStatusException

