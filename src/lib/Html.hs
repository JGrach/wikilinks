{-# LANGUAGE OverloadedStrings #-}

module Html(extractTagById, extractTagInDepth, extractLinkTags, extractHrefAttr) where 

import Data.Text ( Text, isPrefixOf, concat )
import Data.Functor((<&>))
import Text.Regex.TDFA((=~))
import Exception (ExceptionFlag (InternalException))

extractTagById:: Text -> Text -> Either ExceptionFlag (Maybe Text)
extractTagById "" _ = Right Nothing
extractTagById _ "" = Right Nothing
extractTagById tagId html = case html =~ ("<([a-z]+)[^>]*id=\"" <> tagId <> "\"[^>]*>"::Text):: (Text, Text, Text, [Text]) of
    (_, _, _, []) -> Right Nothing
    (_, pattern, next, tag:_) -> extractTagInDepth tag next 1 <&> \x -> Just (pattern <> x)

extractTagInDepth::Text -> Text -> Int -> Either ExceptionFlag Text
extractTagInDepth _ _ 0 = Right ""
extractTagInDepth "" _ _ = Left InternalException
extractTagInDepth _ "" _ = Left InternalException
extractTagInDepth tag html depth = case html =~ ("<" <> tag <> "[^>]*>|<\\/" <> tag <> ">"):: (Text, Text, Text) of
    (_, "", _) -> Left InternalException
    (prev, pattern, next)
        | ("<" <> tag) `isPrefixOf` pattern -> extractTagInDepth tag next (depth + 1) <&> \x -> prev <> pattern <> x
        | "</" <> tag <> ">" == pattern -> extractTagInDepth tag next (depth - 1) <&> \x -> prev <> pattern <> x
        | otherwise -> Left InternalException

extractLinkTags::Text -> [Text]
extractLinkTags text 
    | text == "" = []
    | otherwise = case text =~ ("<a ([^>]*)"::Text):: (Text, Text, Text, [Text]) of 
        (_, _, next, a) -> a ++ extractLinkTags next

extractHrefAttr::Text -> Text 
extractHrefAttr text = case text =~ ("href=\"([^\"]*)"::Text):: (Text, Text, Text, [Text]) of
    (_, _, _, a) -> Data.Text.concat a -- TODO: should only have 1 href but could be false
