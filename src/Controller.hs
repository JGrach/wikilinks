{-# LANGUAGE OverloadedStrings #-}

module Controller (ctrlWikiLinks, ctrlMissingRoute) where

import Data.Text ( isPrefixOf, pack, Text )
import qualified Data.List
import Network.HTTP.Types (Query, status200, status400, status404, status500, hContentType)
import Data.Text.Encoding (decodeUtf8)
import Data.ByteString.Lazy (fromStrict)
import Data.ByteString (ByteString)
import Data.Functor((<&>))

import Network (Response, queryString, responseLBS, Application)
import Exception (ExceptionFlag(ParameterBadFormatException, NotFoundException), fromExceptionFlagToJson)
import qualified Wikilinks(fetchWikiLinks, WikiLink, fromWikiLinksToJson)
import qualified Utils (flat)

ctrlWikiLinks::Application
ctrlWikiLinks request respond =
    Utils.flat (mapM
        Wikilinks.fetchWikiLinks
        (extractWikiUrl (extractUrl $ queryString request))) >>= \a -> respond $ createResponse $ packResponse a

extractUrl:: Query -> Maybe Text
extractUrl q = Data.List.find (\(key, _) -> key == "url") q >>= \(_, value) -> fmap decodeUtf8 value

extractWikiUrl::Maybe Text -> Either ExceptionFlag Text
extractWikiUrl (Just link)
    | pack "https://en.wikipedia.org/wiki/" `Data.Text.isPrefixOf` link = Right link
    | otherwise = Left ParameterBadFormatException
extractWikiUrl Nothing = Left ParameterBadFormatException

packResponse::Either ExceptionFlag [Wikilinks.WikiLink] -> Either ExceptionFlag ByteString
packResponse a = a <&> \b -> Wikilinks.fromWikiLinksToJson b

ctrlMissingRoute::Application
ctrlMissingRoute _ respond =
    respond $ createResponse (Left NotFoundException::Either ExceptionFlag ByteString)

createResponse::Either ExceptionFlag ByteString -> Response
createResponse links = case links of
    Left NotFoundException -> responseLBS 
        status404 [(hContentType, "application/json")] 
        $ "{\"error\":\"" <> fromStrict (fromExceptionFlagToJson NotFoundException) <> "\"}"
    Left ParameterBadFormatException -> responseLBS 
        status400 [(hContentType, "application/json")] 
        $ "{\"error\":\"" <> fromStrict (fromExceptionFlagToJson ParameterBadFormatException) <> "\"}"
    Left x -> responseLBS 
        status500 [(hContentType, "application/json")] 
        $ "{\"error\":\"" <> fromStrict (fromExceptionFlagToJson x) <> "\"}"
    Right x -> responseLBS 
        status200 [(hContentType, "application/json")] 
        $ "{\"results\": " <> fromStrict x <> "}"
