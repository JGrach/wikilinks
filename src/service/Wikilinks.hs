{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

module Wikilinks (fetchWikiLinks, WikiLink, fromWikiLinksToJson) where

import Data.Text.Lazy.Encoding (decodeUtf8)
import Data.Text.Lazy (toStrict)
import Data.Text(Text, isPrefixOf)
import Data.Text.Encoding(encodeUtf8)
import Data.Functor((<&>))
import Data.ByteString (ByteString)

import HttpClient (fetch)
import Exception (ExceptionFlag)
import Html(extractTagById, extractLinkTags, extractHrefAttr)
import Utils (removeDuplicates, join)
import Data.Maybe (fromMaybe)

newtype WikiLink = WikiLink { -- TODO: add Maybe title, etc ...
    url :: Text
}

fetchWikiLinks::Text -> IO (Either ExceptionFlag [WikiLink])
fetchWikiLinks link = fetch link <&> \x -> x >>= \y -> extractArticle (toStrict $ decodeUtf8 y) <&> findArticleLinks

extractArticle::Text -> Either ExceptionFlag Text
extractArticle html = extractTagById "bodyContent" html <&> fromMaybe ""

findArticleLinks:: Text -> [WikiLink]
findArticleLinks article = cleanLinks (extractLinkTags article <&> extractHrefAttr) <&> \link ->  WikiLink { url = link }

cleanLinks:: [Text] -> [Text]
cleanLinks links = removeDuplicates $ Prelude.filter (isPrefixOf "/wiki/") links

fromWikiLinksToJson:: [WikiLink] -> ByteString
fromWikiLinksToJson ws = encodeUtf8 $ "[" <> 
    join (ws <&> \w -> "{\"type\":\"WikiLink\", \"url\":\""<> url w <>"\"}") "," <>
    "]"
