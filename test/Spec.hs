{-# LANGUAGE OverloadedStrings #-}

import Test.Hspec
import Html (extractTagById)

main :: IO ()
main = hspec $ do
  describe "Html.extractTagById" $ do
    it "should extract a tag by id" $ do
      extractTagById "main" "<div><div>one</div><div id=\"main\"><div>two</div></div></div>" 
      `shouldSatisfy` either (const False) (Just "<div id=\"main\"><div>two</div></div>" ==)

    it "should extract a tag by id" $ do
      extractTagById "main" "<div><div>one</div><div><span class=\"something\" id=\"main\">two</span></div></div>" 
      `shouldSatisfy` either (const False) (Just "<span class=\"something\" id=\"main\">two</span>" ==)

    it "should extract Nothing on inexistent id" $ do
      extractTagById "main" "<div><div>one</div><div><div>two</div></div></div>" 
      `shouldSatisfy` either (const False) (Nothing ==) 
      
    it "should extract Nothing on inexistent id" $ do
      extractTagById "main" "" 
      `shouldSatisfy` either (const False) (Nothing ==)

    it "should extract a tag by id even if html is empty after that" $ do
      extractTagById "main" "<div id=\"main\">one</div>" 
      `shouldSatisfy` either (const False) (Just "<div id=\"main\">one</div>" ==)
