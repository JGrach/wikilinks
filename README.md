# wikilinks

## 1. About the project

This project is my first Haskell project. It was working to prepare a job interview. \
Goal of this project is to scrap a wikipedia article to list all links in a json.

- Main.hs initialize server
- /lib contains technical abstraction (haskell utils, api to hexagonal architecture, etc ...)
- Controller.hs check network inputs and format outputs
- /service/Wikilinks is about semantic job

## 2. Installation

I use stack so I assume something like: \
`git clone git@gitlab.com:JGrach/wikilinks.git && cd wikilinks && stack run` \
should be sufficient.

## 3. Usage

After a run you could try to run your browser on: \
`http://localhost:3001/wikilinks?url=https://en.wikipedia.org/wiki/Hera` \
Url parameter wait for a valid wikipedia article url.

## 4. Implementation Choices

I choose to don't use a scrap library to show my capability to work with regex.\
We could just search the only one "main" tag on wikipedia html but it was fun to search a way to extract a tag by id.

Json and Error handler could be middleware over functions of controller. In 5 days I didn't want to waste time on this.

I just test some case of Html.hs module due it was essentially an algorythm. In real case we could also test about rest of file.\
Again, in few time I choose to not focus on test library.

## 5. What Next ?

We could connect this to a graph DataBase to hydrate it and create a graph of all wikipedia links. \
With a listener on a network, we could call this each time someone call on wikipedia. \
That could be a way to have a wikipedia graph without big data contraints.
